import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  likes: number = 0;
  heart_black = '🖤';
  heart_fire  = '❤️‍🔥';
  heart_shiny = '💖';
  index = 0;
  hearts: string[] = ['💜','🧡','💚','🤎','💛','🤍','💙'];
  heart = this.hearts[0];
  public mostrarLikes = false;

  addOne(): void {
    this.mostrarLikes = true;
    this.likes += 1;
    this.index += 1;
    if (this.index > 6)
      this.index = 0;

    if(this.likes % 500 == 0) {       // Cada 500 likes, muestra corazón shiny
      this.heart = this.heart_shiny;
    } else if (this.likes % 100 == 0) { // Cada 100 likes, muestra corazón con fuego
      this.heart = this.heart_fire;
    } else if (this.likes % 50 == 0) {  // Cada 50 likes, muestra corazón negro
      this.heart = this.heart_black;
    } else {
      this.heart = this.hearts[this.index]; // Sino muestra los corazones comunes (menos el rojo)
    }
  }
}
